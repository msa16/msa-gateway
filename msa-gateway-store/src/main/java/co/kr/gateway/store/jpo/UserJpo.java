package co.kr.gateway.store.jpo;

import co.kr.gateway.domain.entity.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "USER_INFO")
public class UserJpo implements Serializable {

	private static final long serialVersionUID = -5318878161053888886L;

	public User toDomain() {
		User cdo = new User();
		cdo.setUserSeq(this.userSeq);
		cdo.setUserId(this.userId);
		cdo.setPassword(this.passwd);
		cdo.setUserRole(this.userRole);
		cdo.setUserName(this.userName);
		cdo.setEmail(this.email);
		cdo.setTel(this.tel);
		cdo.setRegDate(this.regDate);
		cdo.setModifyDate(this.modifyDate);

		return cdo;
	}

	@Id
	@Column(nullable = false, unique = true)
	private int userSeq;

	@Column(length = 50, nullable = false)
	private String userId;

	@Column(length = 100, nullable = false)
	private String passwd;

	@Column(length = 30, nullable = false)
	private String userRole;

	@Column(length = 100, nullable = false)
	private String userName;

	@Column(length = 100)
	private String email;

	@Column(length = 100)
	private String tel;

	@Column
	private Date regDate;

	@Column
	private Date modifyDate;

}
