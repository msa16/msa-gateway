package co.kr.gateway.store;

import co.kr.gateway.domain.entity.User;
import co.kr.gateway.domain.store.UserStore;
import co.kr.gateway.store.jpo.UserJpo;
import co.kr.gateway.store.repository.UserJpaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserJpaStore implements UserStore {

	@Autowired
	private UserJpaRepository userJpaRepository;

	@Override
	public User findByUserId(String userId) {
		UserJpo userJpo = userJpaRepository.findByUserId(userId).orElse(null);
		if (userJpo != null) {
			return userJpo.toDomain();
		}
		return null;
	}

	@Override
	public User passwdChange(String userId, String userPass) {
		userJpaRepository.passwdChange(userId, userPass);
		UserJpo userJpo = userJpaRepository.findByUserId(userId).orElse(null);

		return userJpo.toDomain();
	}
}
