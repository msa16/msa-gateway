package co.kr.gateway.store.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import co.kr.gateway.store.jpo.UserJpo;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

public interface UserJpaRepository extends JpaRepository<UserJpo, String> {
	Optional<UserJpo> findByUserId(String userId);

	@Transactional
	@Modifying
	@Query("UPDATE UserJpo a SET a.userPass=?2 WHERE a.userId=?1")
	int passwdChange(String userId, String userPass);

}
