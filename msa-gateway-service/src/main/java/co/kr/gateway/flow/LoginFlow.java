package co.kr.gateway.flow;

import co.kr.gateway.domain.facade.LoginFlowFacade;
import co.kr.gateway.domain.service.LoginService;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@Service
@Slf4j
public class LoginFlow implements LoginFlowFacade {

	@Autowired
	private LoginService loginService;

	@Override
	public Map<String, String> login(HttpServletRequest request, String userId, String password) {

		Map<String, String> result = getResult();
		try {
			String token = loginService.login(request, userId, password);
			result.put("token", token);
		} catch (Exception e){
			getErrorResult(result, e);
			log.error("login Error!", e);
		}

		return result;
	}

	@Override
	public Map<String, String> checkToken(String token) {
		Map<String, String> result = getResult();
		try {
			result.put("result", loginService.checkToken(token) == true ? "success" : "fail");
		} catch (Exception e){
			getErrorResult(result, e);
			log.error("checkToken Error!", e);
		}

		return result;
	}

	@Override
	public Map<String, String> passwdChange(HttpServletRequest request, String userId, String oldPasswd, String newPasswd) {
		Map<String, String> result = getResult();
		try {
			String token = loginService.passwdChange(request, userId, oldPasswd, newPasswd);
			result.put("token", token);
		} catch (Exception e){
			getErrorResult(result, e);
			log.error("passwdChange Error!", e);
		}

		return result;
	}

	private Map<String, String> getResult(){
		Map<String, String> result = new HashMap<String, String>();
		result.put("resultCode", "Y");

		return result;
	}

	private void getErrorResult(Map<String, String> result, Exception e){
		result.put("resultCode", "N");
		result.put("errorMsg", e.getMessage());
	}

}