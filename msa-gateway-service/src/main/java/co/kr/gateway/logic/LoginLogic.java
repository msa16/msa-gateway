package co.kr.gateway.logic;

import co.kr.gateway.config.security.JwtTokenProvider;
import co.kr.gateway.domain.entity.User;
import co.kr.gateway.domain.service.LoginService;
import co.kr.gateway.domain.store.UserStore;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@Service
public class LoginLogic implements LoginService {
	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private JwtTokenProvider jwtTokenProvider;

	@Autowired
	private UserStore userStore;

	@Override
	public String login(HttpServletRequest request, String userId, String password) {

		User member = userStore.findByUserId(userId);

		String errMsg = null;
		if (member == null) {
			errMsg = "[Login] Not registered id";
		}

		if (errMsg == null && !passwordEncoder.matches(password, member.getPassword())) {
			errMsg = "[Login] Invalid password";
		}

		if(errMsg != null){
			throw new IllegalArgumentException(errMsg);
		}

		return jwtTokenProvider.createToken(member);
	}

	@Override
	public boolean checkToken(String token) {
		return jwtTokenProvider.validateToken(token);
	}

	@Override
	public String passwdChange(HttpServletRequest request, String userId, String oldPasswd, String newPasswd) {

		User member = userStore.findByUserId(userId);

		String errMsg = null;
		if (member == null) {
			errMsg = "[Modify] Not registered id";
		}

		if (errMsg == null && !passwordEncoder.matches(oldPasswd, member.getPassword())) {
			errMsg = "[Modify] Invalid password";
		}

		if(errMsg != null){
			throw new IllegalArgumentException(errMsg);
		}

		member = userStore.passwdChange(member.getUserId(), passwordEncoder.encode(newPasswd));

		return jwtTokenProvider.createToken(member);
	}
}
