package co.kr.gateway.rest;

import co.kr.gateway.domain.facade.LoginFlowFacade;
import co.kr.gateway.flow.LoginFlow;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@RestController
@CrossOrigin("*")
@RequestMapping("/msa/v1.0")
@Api(value = "/msa/v1.0", tags = "login, jwt check, passwd change resource")
public class LoginFlowResource implements LoginFlowFacade {

	@Autowired
	private LoginFlow loginFlow;

	@Override
	@ApiOperation(value = "MSA-GATEWAY-001", notes = "사용자 로그인")
	@PostMapping("/login")
	public Map<String, String> login(HttpServletRequest request, String userId, String password) {
		return loginFlow.login(request, userId, password);
	}

	@Override
	@ApiOperation(value = "MSA-GATEWAY-002", notes = "토큰 검증")
	@GetMapping("/token")
	public Map<String, String> checkToken(String token) {
		return loginFlow.checkToken(token);
	}

	@Override
	@ApiOperation(value = "MSA-GATEWAY-003", notes = "비밀번호 변경")
	@PutMapping("/passwd")
	public Map<String, String> passwdChange(HttpServletRequest request, String userId, String oldPasswd, String newPasswd) {
		return loginFlow.passwdChange(request, userId, oldPasswd, newPasswd);
	}

}