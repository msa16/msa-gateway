#!/bin/sh

nohup java -Dspring.config.location=./config/application.properties -Djava.library.path=/usr/lib -jar ./msa-gateway-boot-1.0.jar -n msa-gateway 1>/dev/null 2>&1 &