package co.kr.gateway.config.security;

import co.kr.gateway.domain.entity.Token;
import co.kr.gateway.domain.entity.User;
import io.jsonwebtoken.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.oauth2.provider.token.store.KeyStoreKeyFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import java.security.KeyPair;
import java.util.Base64;
import java.util.Date;
import java.util.UUID;

@Slf4j
@Component
public class JwtTokenProvider {

    @Value("${jwt.req.name:X-AUTH-TOKEN}")
    private String name;

    @Value("${jwt.alg:HS256}")
    private String algorithm;

    @Value("${jwt.keystore.name}")
    private String keystoreName;
    @Value("${jwt.keystore.secret}")
    private String keystoreSecret;

    @Value("${jwt.key.alias}")
    private String keyAlias;
    @Value("${jwt.sign.secret}")
    private String signSecret;
    @Value("${jwt.key.secret}")
    private String keySecret;

    /**
     * valid time 5min
     */
    @Value("${jwt.valid-time:300000}")
    private long userTokenValidTime;

    @PostConstruct
    protected void init() {
        signSecret = Base64.getEncoder().encodeToString(signSecret.getBytes());
    }

    public String createToken(User user) {
        Date now = new Date();
        Token t = Token.builder()
                .tokenId(UUID.randomUUID().toString().replace("-", ""))
                .issuedAt(now)
                .expiration(new Date(now.getTime() + userTokenValidTime))
                .audience(user.getUserId())
                .role(StringUtils.arrayToCommaDelimitedString(StringUtils.commaDelimitedListToStringArray(user.getUserRole())))
                .userSeq(user.getUserSeq())
                .build();

        Claims claims = Jwts.claims().setSubject(t.getAudience());
        claims.put("role", "ROLE_" + t.getRole());
        claims.put("seq", t.getUserSeq());

        log.debug("role, {}", t.getRole());

        JwtBuilder jwtBuilder = Jwts.builder();
        jwtBuilder
                .setClaims(claims)
                .setIssuedAt(t.getIssuedAt())
                .setExpiration(t.getExpiration())
                .setId(t.getTokenId())
                .setAudience(t.getAudience());

        if (algorithm.equals(SignatureAlgorithm.HS256.getValue()))
            return jwtBuilder.signWith(SignatureAlgorithm.HS256, signSecret).compact();
        else
            return jwtBuilder.signWith(SignatureAlgorithm.RS256, getKeyPair().getPrivate()).compact();
    }

    public Authentication getAuthentication(String token) {
        Token t = getToken(token);
        UserDetails userDetails = User.builder()
                .userId(t.getAudience())
                .userRole(t.getRole())
                .build();
        return new UsernamePasswordAuthenticationToken(userDetails, "", userDetails.getAuthorities());
    }

    public Token getToken(String token) {
        Claims body = algorithm.equals(SignatureAlgorithm.HS256.getValue())
                ?Jwts.parser().setSigningKey(signSecret).parseClaimsJws(token).getBody()
                :Jwts.parser().setSigningKey(getKeyPair().getPublic()).parseClaimsJws(token).getBody();

        Token t = Token.builder()
                .tokenId(body.getId())
                .issuedAt(body.getIssuedAt())
                .expiration(body.getExpiration())
                .audience(body.getAudience())
                .role(body.get("role").toString())
                .build();
        return t;
    }

    public String resolveToken(HttpServletRequest request) {
        //TODO Header Key 값이 소문자로 들어옴. Tomcat 버그(https://github.com/mitre/HTTP-Proxy-Servlet/issues/65)
        return request.getHeader(name);
    }

    public boolean validateToken(String jwtToken) {
        try {
            Jws<Claims> claims = algorithm.equals(SignatureAlgorithm.HS256.getValue())
                    ?Jwts.parser().setSigningKey(signSecret).parseClaimsJws(jwtToken)
                    :Jwts.parser().setSigningKey(getKeyPair().getPublic()).parseClaimsJws(jwtToken);
            return !claims.getBody().getExpiration().before(new Date());
        } catch (Exception e) {
            return false;
        }
    }

    public KeyPair getKeyPair() {
        KeyPair keyPair = new KeyStoreKeyFactory(new ClassPathResource(keystoreName), keystoreSecret.toCharArray()).getKeyPair(keyAlias, keySecret.toCharArray());
        return keyPair;
    }
}