package co.kr.gateway.config.security;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.util.StringUtils;

import java.util.List;

@Slf4j
@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
@PropertySource("classpath:security.properties")
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private JwtTokenProvider jwtTokenProvider;

	@Value("#{'${security.auth}'.split(',')}")
	private String[] auth;

	@Value("#{'${security.role}'.split(',')}")
	private String[] role;

	@Bean
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.httpBasic().disable()
				.csrf().disable()
				.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

		http.authorizeRequests().antMatchers("/swagger-ui.html/**").permitAll();
		http.authorizeRequests().antMatchers("/msa/v1.0/login").permitAll();
		http.authorizeRequests().antMatchers("/msa/v1.0/token").permitAll();
		//http.authorizeRequests().antMatchers("/error/**").permitAll();
		for (String str : auth) {
			String[] split = StringUtils.delimitedListToStringArray(str, ":");
			if(split.length == 1){
				log.debug("path: {}, role: {}",split[0], role);
				http.authorizeRequests().antMatchers(split[0]).hasAnyRole(role);
			} else if (split.length == 2) {
				log.debug("path: {}, role: {}",split[0], split[1]);
				if (split[1].contains("ALL")) {
					http.authorizeRequests().antMatchers(split[0]).permitAll();
				} else {
					http.authorizeRequests().antMatchers(split[0]).hasRole(split[1]);
				}
			} else if (split.length == 3) {
				log.debug("method: {}, path: {}, role: {}",split[0], split[1], split[2]);
				HttpMethod method = HttpMethod.valueOf(split[0].toUpperCase());
				if (split[2].toUpperCase().contains("ALL")) {
					http.authorizeRequests().antMatchers(method, split[1]).permitAll();
				} else {
					http.authorizeRequests().antMatchers(method, split[1]).hasRole(split[2]);
				}
			}
		}

		http.addFilterBefore(new JwtAuthenticationFilter(jwtTokenProvider), UsernamePasswordAuthenticationFilter.class);
	}
}