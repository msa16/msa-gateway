package co.kr.gateway.config.security;

import co.kr.gateway.domain.entity.User;
import co.kr.gateway.domain.store.UserStore;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class CustomUserDetailService implements UserDetailsService {

	@Autowired
	private UserStore userStore;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = userStore.findByUserId(username);
		if (user == null) {
			throw new UsernameNotFoundException("Fail to find user");
		}
		return user;
	}
}