package co.kr.gateway.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.RequestMethod;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import javax.servlet.http.HttpSession;
import java.security.Principal;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

	@Bean
	public Docket swaggerSpringfoxDocket() {
		Docket docket = new Docket(DocumentationType.SWAGGER_2)
				.apiInfo(apiEndPointsInfo())
				.pathMapping("/")
				.forCodeGeneration(true)
				.genericModelSubstitutes(ResponseEntity.class)
				.ignoredParameterTypes(Authentication.class, HttpSession.class, Principal.class)
				.securityContexts(Collections.singletonList(securityContext()))
				.securitySchemes(Collections.singletonList(apiKey()))
				.useDefaultResponseMessages(false);
		docket = docket.select()
				.apis(RequestHandlerSelectors.basePackage("co.kr.gateway.rest"))
				.paths(PathSelectors.any()).build();
		
		List<ResponseMessage> responseMessages = Arrays.asList(
		        new ResponseMessageBuilder().code(200).message("OK").build(),
		        new ResponseMessageBuilder().code(401).message("Unauthorized").build(),
		        new ResponseMessageBuilder().code(403).message("Forbidden").build(),
		        new ResponseMessageBuilder().code(404).message("NotFound").build(),
		        new ResponseMessageBuilder().code(408).message("Request Timeout").build(),
		new ResponseMessageBuilder().code(500).message("Internal Server Error").build(),
		new ResponseMessageBuilder().code(504).message("Gateway Timeout").build()
		);
		docket.globalResponseMessage(RequestMethod.PUT, responseMessages);
		docket.globalResponseMessage(RequestMethod.POST, responseMessages);
		docket.globalResponseMessage(RequestMethod.GET, responseMessages);
		docket.globalResponseMessage(RequestMethod.DELETE, responseMessages);
		
		return docket;
	}

	private ApiInfo apiEndPointsInfo() {
		return new ApiInfoBuilder().title("AUTH REST API")
				.description("This documents describes about auth version 1 REST API")
//				.contact(new Contact("Jay Song", "", "jay.song@gmail.com"))
				.license("Apache 2.0")
				.version("v2")
				.build();
	}

	private ApiKey apiKey() {
		return new ApiKey("JWT", "X-AUTH-TOKEN", "header");
	}

	private springfox.documentation.spi.service.contexts.SecurityContext securityContext() {
		return springfox.documentation.spi.service.contexts.SecurityContext.builder().securityReferences(defaultAuth()).forPaths(PathSelectors.any()).build();
	}

	List<SecurityReference> defaultAuth() {
		AuthorizationScope authorizationScope = new AuthorizationScope("global", "accessEverything");
		AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
		authorizationScopes[0] = authorizationScope;
		return Collections.singletonList(new SecurityReference("JWT", authorizationScopes));
	}

}
