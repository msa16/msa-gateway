package co.kr.gateway.common.domain;

import co.kr.gateway.common.json.JsonSerializable;
import co.kr.gateway.common.json.JsonUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Objects;
import java.util.StringTokenizer;

@Getter
@Setter
@NoArgsConstructor
public class NameValue implements JsonSerializable {
    //
    private String name;
    private String value;

    public NameValue(String name, String value) {
        //
        this.setName(name);
        this.setValue(value);
    }

    public static NameValue fromJson(String json) {
        //
        return JsonUtil.fromJson(json, NameValue.class);
    }

    @Override
    public String toString() {
        //
        return toJson();
    }

    @Override
    public boolean equals(Object target) {
        //
        if (this == target) {
            return true;
        }

        if (target == null || getClass() != target.getClass()) {
            return false;
        }

        NameValue nameValue = (NameValue)target;
        return Objects.equals(this.name, nameValue.name)
                && Objects.equals(this.value, nameValue.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name+value);
    }

    public String toSimpleString() {
        //
        return String.format("%s:%s", name, value);
    }

    public static NameValue fromSimpleString(String nameValueString) {
        //
        StringTokenizer tokenizer = new StringTokenizer(nameValueString, ":");
        String name = tokenizer.nextToken();
        String value = tokenizer.nextToken();

        return new NameValue(name, value);
    }

}
