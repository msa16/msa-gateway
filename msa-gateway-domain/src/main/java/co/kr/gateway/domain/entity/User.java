package co.kr.gateway.domain.entity;

import co.kr.gateway.common.domain.NameValue;
import co.kr.gateway.common.domain.NameValueList;
import co.kr.gateway.common.json.JsonUtil;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.StringUtils;

import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.stream.Collectors;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class User implements UserDetails {

	private static final long serialVersionUID = 2863408751430021949L;

	private int userSeq;

	private String userId;

	private String password;

	private String userRole;

	private String userName;

	private String email;

	private String tel;

	private Date regDate;

	private Date modifyDate;

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return Arrays.asList(StringUtils.commaDelimitedListToStringArray(this.userRole)).stream().map(SimpleGrantedAuthority::new).collect(Collectors.toList());
	}

	@Override
	public String getUsername() {
		return userId;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}
	
	public void setValues(NameValueList nameValues) {
		for (NameValue nameValue : nameValues.list()) {
			String value = nameValue.getValue();
			switch (nameValue.getName()) {
				case "userSeq":
					this.userSeq = Integer.parseInt(value);
					break;
				case "userId":
					this.userId = value;
				break;
				case "userRole":
					this.userRole = value;
				break;
				case "regDate":
					this.regDate = new Date(value);
					break;
				case "modifyDate":
					this.modifyDate = new Date(value);
				break;
			}
		}
	}

	public static User fromJson(String json) {
		return JsonUtil.fromJson(json, User.class);
	}

}
