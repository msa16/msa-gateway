package co.kr.gateway.domain.store;

import co.kr.gateway.domain.entity.User;

public interface UserStore {

	User findByUserId(String userId);

	User passwdChange(String userId, String userPass);

}