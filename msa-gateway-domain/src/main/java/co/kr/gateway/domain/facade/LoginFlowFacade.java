package co.kr.gateway.domain.facade;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

public interface LoginFlowFacade {

	Map<String, String> login(HttpServletRequest request, String userId, String password);

	Map<String, String> checkToken(String token);

	Map<String, String> passwdChange(HttpServletRequest request, String userId, String oldPasswd, String newPasswd);
}