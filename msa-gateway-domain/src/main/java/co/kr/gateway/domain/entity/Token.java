package co.kr.gateway.domain.entity;

import java.io.Serializable;
import java.util.Date;

import co.kr.gateway.common.domain.NameValue;
import co.kr.gateway.common.domain.NameValueList;
import co.kr.gateway.common.json.JsonUtil;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Token implements Serializable {

	private static final long serialVersionUID = -8013486474781575677L;

	private String tokenId;

	private Date issuedAt;

	private Date expiration;

	private String audience;

	private String role;

	private int userSeq;

	public void setValues(NameValueList nameValues) {
		for (NameValue nameValue : nameValues.list()) {
			String value = nameValue.getValue();
			switch (nameValue.getName()) {
				case "tokenId":
					this.tokenId = value;
				break;
				case "issuedAt":
					this.issuedAt = new Date(value);
				break;
				case "expiration":
					this.expiration = new Date(value);
				break;
				case "audience":
					this.audience = value;
				break;
				case "role":
					this.role = value;
				break;
				case "userSeq":
					this.userSeq = Integer.parseInt(value);
				break;
			}
		}
	}

	public static Token fromJson(String json) {
		return JsonUtil.fromJson(json, Token.class);
	}

}
