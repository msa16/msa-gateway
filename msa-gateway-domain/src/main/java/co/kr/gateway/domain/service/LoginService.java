package co.kr.gateway.domain.service;

import javax.servlet.http.HttpServletRequest;

public interface LoginService {

	String login(HttpServletRequest request, String userId, String password);

	boolean checkToken(String token);

	String passwdChange(HttpServletRequest request, String userId, String oldPasswd, String newPasswd);
}